import React from 'react'
// import AppNavBar from '../components/AppNavBar';


const Home = () =>  {
  return (
    <div classNameName="hero"> 
        <div className="card bg-dark text-white border-0">
          <img className="card-img" src="/bg5.jpg" alt="Background" height="800px" />
          <div className="card-img-overlay d-flex flex-column justify-content-center ">
              <div classNameName="container"> 
              <h5 className="card-title display-3 fw-bolder mb-0">NEW SEASON ARRIVALS</h5>
            <p className="card-text lead fs-4 fw-bolder">
              CHECK OUT ALL THE TRENDS
            </p>
              </div>
          </div>
      </div>
    </div>
  );
}

export default Home;