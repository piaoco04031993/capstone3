
import { useState, useEffect, useContext } from 'react';
import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
const data = {
	title: 'Welcome to the Create Product Page',
	content: 'Create Product on this page'
};

export default function Create () {
	const { user } = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [active, setActive] = useState('');
	let token = localStorage.getItem('accessToken');
	useEffect(() => {
		if (name !== '' && description !== '' && price !== 0) {
			setActive(true);
		} else {
			setActive(false);
		}
	},[name, price, description])
	const createProduct = async (event) => {
		event.preventDefault();

		const isCreated = await fetch('https://whispering-gorge-43888.herokuapp.com/products/create', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},

			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
			if (data) {
				return true
			} else {
				return false
			}
		})
		if (isCreated) {
			await Swal.fire({
				icon: 'success',
				title: ' Product Created Successfully!'
			});
			setName('');
			setPrice(0);
			setDescription('');
			/*.push('/products')*/
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Contact IT Department!'
			})
		}
	
	};

	return(
		user.id && user.isAdmin
		?
		<>
			<Hero bannerData={data}/>
			<Container>
				<h1 className="text-center">Create Product Form</h1>
				<Form onSubmit={e => createProduct(e)}>
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter Product Name" value={name} onChange={event => setName(event.target.value)} required />
					</Form.Group>

					{/*Description Field*/}
					<Form.Group>
						<Form.Label>Description: </Form.Label>
						<Form.Control type="text" placeholder="Enter Description" value={description} onChange={event => setDescription(event.target.value)} required />
					</Form.Group>

					
					{/*Price*/}
					<Form.Group>
						<Form.Label>Price: </Form.Label>
						<Form.Control type="number" placeholder="Enter Price" value={price} onChange={event => setPrice(event.target.value)} required />
					</Form.Group>

					{/*Create Course Button*/}
					
					{
						active ?
							<Button variant="success" className="btn-block" type="submit">Create
							</Button>
						:
							<Button variant="success" className="btn-block" disabled>Create
							</Button>
					}

				</Form>
			</Container>
		</>
		:
		<Navigate to="/products" replace={true} />
		);

}