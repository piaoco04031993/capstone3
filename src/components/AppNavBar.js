
import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';

function AppNavBar(){
	const { user } = useContext(UserContext);
	return(
		<Navbar bg="darkcyan" expand="lg" className=''>
			<Container>
				<Navbar.Brand className="text-dark"> MY SHOP </Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse>
					<Nav className="ml-auto">
						<Link to='/' className="nav-link text-dark">
							Home
						</Link>
						{
							user.id !== null ?
								<Link to='/logout' className="nav-link text-dark">
									Logout
								</Link>
							:
								<>
									<Link to='/register' className="nav-link text-dark">
										Register
									</Link>
									<Link to='/login' className="nav-link text-dark">
										Login
									</Link>
								</>
						}
						<Link to='/products' className="nav-link text-dark">
							Products
						</Link>
						<Link to='/products/create' className="nav-link text-dark">
							Create Product
						</Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
		);
};



export default AppNavBar;
