//Grid System and Card
import {Row, Col, Card, Container} from 'react-bootstrap'

//format the card with the help of utility classes of the bootstrap
export default function Highlights (){
	return(
		<Container>
			<Row className="my-3">
			{/*1st Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> aaa </Card.Title>
							<Card.Text>
								
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

			{/*2nd Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> aaa </Card.Title>
							<Card.Text>
							
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

			{/*3rd Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> aaa</Card.Title>
							<Card.Text>
								
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>	
		</Container>
		);
}